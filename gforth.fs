2variable timer
2variable diff0

: timer-restart ( -- )  utime timer 2! ;
: timer-reset ( -- )    0. diff0 2!  timer-restart ;
: timer-hold ( -- )     utime timer 2@ d- diff0 2@ d+ diff0 2! ;
: timer-preset ( u -- ) 1000 m* diff0 2!  timer-restart ;

: (.t0) ( -- c-addr u )
  utime timer 2@ d- diff0 2@ d+ 1000 fm/mod nip
  0 <# # # # [char] . hold #s #> ;

: .elapsed ( -- )  (.t0) type ."  seconds elapsed." ;
: .ms ( -- )       (.t0) type ."  seconds." ;
