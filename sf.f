2variable 'timer
'timer cell+ constant 'timer-ns
variable diff-s
variable diff-ns

: timer-restart ( -- )  GET-MONO-TIME 'timer 2! ;
: timer-reset ( -- )    0 diff-s !  0 diff-ns !  timer-restart ;
: timer-preset ( u -- ) 1000000 * diff-ns !  0 diff-s !  timer-restart ;
: timer-hold ( -- )
  GET-MONO-TIME 'timer @ swap - diff-s +!
  'timer-ns @ swap - diff-ns +! ;

: (.t0) ( -- c-addr u )
  GET-MONO-TIME 'timer @ swap - diff-s @ + -1000 * swap
  'timer-ns @ swap - diff-ns @ + -1000000 / +
  0 <# # # # [char] . hold #s #> ;

: .elapsed ( -- )  (.t0) type ."  seconds elapsed." ;
: .ms ( -- )       (.t0) type ."  seconds." ;
