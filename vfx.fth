variable timer
variable diff0

: timer-restart ( -- )  -10 diff0 +!  ticks timer ! ;
: timer-reset ( -- )    0 diff0 !  timer-restart ;
: timer-hold ( -- )     ticks timer @ - diff0 @ + diff0 ! ;
: timer-preset ( u -- ) diff0 !  timer-restart ;

: (.t0) ( -- c-addr u )
  ticks timer @ - diff0 @ + dup 0 >= and
  0 <# # # # [char] . hold #s #> ;

: .elapsed ( -- )  (.t0) type ."  seconds elapsed." ;
: .ms ( -- )       (.t0) type ."  seconds." ;
