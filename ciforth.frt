WANT D- TICKS 2VARIABLE TICKS-PER-SECOND

TICKS-PER-SECOND 1000 / CONSTANT TICKS-PER-MS
 
2VARIABLE TIMER
2VARIABLE DIFF0

: TIMER-RESTART ( -- )  TICKS TIMER 2! ;
: TIMER-RESET ( -- )    0. DIFF0 2!  TIMER-RESTART ;
: TIMER-HOLD ( -- )     TICKS TIMER 2@ D- DIFF0 2@ D+ DIFF0 2! ;
: TIMER-PRESET ( U -- ) TICKS-PER-MS M* DIFF0 2!  TIMER-RESTART ;

: (.T0) ( -- c-addr u )
  TICKS TIMER 2@ D- DIFF0 2@ D+ TICKS-PER-MS FM/MOD NIP
  0 <# # # # [CHAR] . HOLD #S #> ;

: .ELAPSED ( -- )  (.T0) TYPE ."  seconds elapsed." ;
: .MS ( -- )       (.T0) TYPE ."  seconds." ;
